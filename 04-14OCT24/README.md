# Fourth class, 14 October 2024

Complementation of Büchi-recognizable languages.

The directed acyclic graph of runs of a Büchi automaton on an infinite word.

Theorem. The complement of a Büchi-recognizable language is Büchi-recognizable [Finkbeiner: Theorem 5.1]

Finkbeiner Chapter 5.
