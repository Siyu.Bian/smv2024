# Sixth class, 28 October 2024

QPTL: LTL + quantifiers.

Theorem. Every LTL-recognizable language is non-counting.

(However, some QPTL languages are counting).

Theorem. Every Büchi-recognisable language in QPTL-definable.

Finkbeiner Chapter 7.
