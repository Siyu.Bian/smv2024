# Third class, 7 October 2024

Equivalence of ω-regular languages and Büchi-recognizable [Finkbeiner: Theorem 3.6]

Deterministic Büchi-recognizable languages

Characterization as closures of classical regular languages [Finkbeiner: Theorem 4.2]

Complementation of deterministic Büchi-recognizable languages: not necessarily deterministic, but always recognizable [Finkbeiner: Theorem 4.3]

Finkbeiner Chapter 3, 4.
