# Modeling and Verification @ [University of Geneva](http://www.unige.ch)

This repository contains important information about this course.
**Do not forget to [watch](https://gitlab.unige.ch/Laurent.Bartholdi/smv2024) it** as it will allow us to send notifications for events, such as new exercises, homework, slides or fixes.

## Important Information and Links

* [Page on Gitlab: `https://gitlab.unige.ch/Laurent.Bartholdi/smv2024`](https://gitlab.unige.ch/Laurent.Bartholdi/smv2024)
* Courses are Monday 8:15 - 10:00
* Exercises are Monday 10:15 - 12:00
* You have free time on Friday 14:00 - 16:00 to work on the course/exercises/homeworks. During this period, the teacher and the assistant do not come. This is personal time for you.
* Team: Laurent Bartholdi (teacher), Aurélien Coet (assistant)
* Use Gitlab issues to communicate with us (public)
* For private matters send an email or come see us in our offices:
    * `laurent.bartholdi@gmail.com`
    * `aurelien.coet@unige.ch` (office 216)
* During this semester, you will have graded homework which will count for 50% of the final grade. The other 50% will be an exam.
* I include some very good lecture notes by Bernd Finkbeiner, `Automata, Games, and Verification`, on which the course will be loosely based.
* A more advanced text on the material is the book `Erich Grädel, Wolfgang Thomas, Thomas Wilke (Eds.) Automata, Logics, and Infinite Games: A Guide to Current Research. Lecture Notes in Computer Science 2500` It is included here for your convenience, but should not to be distributed.

## Rules

* You must do your homework in your private fork of the course repository.
* If for any reason you have trouble with the deadline,
  contact your teacher as soon as possible.
* The assistants must have access to your source code, but nobody else should have.
* Unless **explicitly** stated, homework is personal work. No collaboration, joint work or sharing of code will be tolerated. You can however discuss general approaches with your colleagues.

## Homework
* All homeworks will be located in a `Homework` directory.
* Do **not rename** any files, variables, functions, classes, ... unless you are instructed to do so!
* Read the complete instructions **before** starting an assignment
* Follow the instructions given to you in the assignments

### Homework Deadlines
You have until 23:59:59 on these dates to **push** your solutions to *your* (private) repository.
Generally: No late submissions, no extensions, no exceptions, no dogs eating homework.
If you are in an unfortunate circumstance where you do need an extension, tell us beforehand.
(Sending an email two hours before the deadline is *not* beforehand).

| No.  |     1     |    2     |    3      |    4     |    5     |    6     |
| ---- | --------- | -------- | --------- | -------- | -------- | -------- |
| Date | TBD       | TBD      | TBD       | TBD      | TBD      | TBD      |

### Tokens (Bonus)

For the whole semester, you have 2 tokens to give you extra times for your TPs.
One token equals an additional 24 hours to complete your TP.
A token is automatically consumed if you push your work after the deadline (even for a second) !

### Evaluation

 * Your final TP grade is the average of all of them.
 * Here is an example of the calculation.

   | TP1 | TP2 | TP3 | TP4 | TP5 | TP6 |
   | --- | --- | --- | --- | --- | --- |
   |  4  |  5  |  4  |  5  |  6  |  5  |

   Final TP grade = (4 + 5 + 4 + 5 + 6 + 5) / 6 = 4.85

### Getting Help

You should be old and smart enough to find the solutions to most problems by yourself. I.e.:  
*If you encounter any problems, solve them!*

In case there is a problem you cannot solve, here is the order of escalation:
  1. Google is your friend. (response time < 1 sec.)
  2. Read the manual. (response time < 1 min.)
  3. Ask a friend/colleague. (response time < 30 mins.)
  4. Stackoverflow. [Learn how](https://stackoverflow.com/help/how-to-ask) (response time < 12 hrs.)
  5. Course assistants. (response time < 2 days.)
  6. Professor. (response time ???)

