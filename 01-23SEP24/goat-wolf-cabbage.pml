bit ferryman=0,goat=0,cabbage=0,wolf=0 ;
proctype Ferryman() {
do
:: (ferryman == goat) -> atomic {ferryman=1-goat; goat=1-goat}
:: (ferryman == wolf) -> atomic {ferryman=1-wolf; wolf=1-wolf}
:: (ferryman == cabbage) -> atomic {ferryman=1-cabbage; cabbage=1-cabbage}
:: ferryman=1-ferryman
:: (ferryman==wolf && wolf==cabbage && cabbage==goat && goat==1) -> goto accept_solution
od
accept_solution: skip
}
init{ run Ferryman() }
