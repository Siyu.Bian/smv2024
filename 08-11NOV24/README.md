# Eigth class, 11 November 2024

Negative theorem: in S1S, addition of terms is not definable.

Presburger arithmetic: a first-order theory with 0, 1, + operations.

Theorem. Presburger arithmetic is decidable.
Proof. Interpret numbers in binary, and represent them as sets in S1S.

Büchi arithmetic: Presburger + valuation predicate V_2(x) = 2^n if x = 2^n*odd. Also decidable.

Automatic sequences: a sequence in Σ^ω whose letters are produced by a DFAO reading the index in binary.

Theorem. Büchi arithmetic + 2-automatic sequences is decidable. (meaning: for every 2-automatic sequence α, every letter a∈Σ and every term t we have the predicate "α[t] = a" at our disposal).

Examples of software: Pecan, Walnut. Lab practice with them.

Pecan can be installed from https://github.com/ReedOei/Pecan

Walnut can be installed from https://github.com/firetto/Walnut (don't use a too recent java)
